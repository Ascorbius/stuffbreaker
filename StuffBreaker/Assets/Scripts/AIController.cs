﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using System.Threading.Tasks;

public class AIController : MonoBehaviour
{
    [SerializeField] float MinPosition = -14f;
    [SerializeField] float MaxPosition = 14f;
    [SerializeField] float BatSpeed = 30f;
    [SerializeField] float deadzone = 1f;

    [SerializeField] float MinFireAngle = 45f;
    [SerializeField] float MaxFireAngle = 135f;
    [SerializeField] float LaunchForce = 800f;

    [SerializeField] GameObject ball;

    // Start is called before the first frame update
    void Start()
    {
        ballLaunch();
    }

    // Update is called once per frame
    void Update()
    {
        batTranslation();
    }

    private void ballLaunch()
    {
        if (ball == null)
        {
            print("We have no ball");
            return;
        }

        var ballController = ball.GetComponent<BallController>();

        float launchAngle = Random.Range(MinFireAngle, MaxFireAngle);
        print("launch angle is:" + launchAngle.ToString());
        float xForce = LaunchForce * Mathf.Cos(Mathf.Deg2Rad * launchAngle);
        float yForce = LaunchForce * Mathf.Sin(Mathf.Deg2Rad * launchAngle);
        ballController.LaunchBall(new Vector3(xForce, yForce, 0));
  
    }

    private void batTranslation()
    {
        
        float yPosition = transform.localPosition.y;
        float zPosition = transform.localPosition.z;
        float xOffset = 0;

        if (ball.transform.localPosition.x > transform.localPosition.x + deadzone)
        {
            xOffset = 1f * BatSpeed * Time.deltaTime;
        }
        else if (ball.transform.localPosition.x < transform.localPosition.x - deadzone)
        {
            xOffset = -1f * BatSpeed * Time.deltaTime;
        }

        float rawXPosition = Mathf.Clamp((transform.localPosition.x + xOffset), MinPosition, MaxPosition);

        transform.localPosition = new Vector3(rawXPosition, yPosition, zPosition);
    }
}
