﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    private static AudioController _instance;

    public static AudioController MainAudioController { get { return _instance; } }
    public AudioSource audioSource;

    // Start is called before the first frame update
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            _instance.audioSource = _instance.gameObject.GetComponent<AudioSource>();
        }
    }

    public void PlaySound( AudioClip soundToPlay)
    {
        audioSource.PlayOneShot(soundToPlay);
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
