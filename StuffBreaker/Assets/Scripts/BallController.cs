﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BallController : MonoBehaviour
{
    private bool launched = false;
    private Vector3 launchForce;
    private Rigidbody rigidBody;
    [SerializeField] List<AudioClip> BounceSounds;
    [SerializeField] float contactAngle;
    [SerializeField] Mesh SpikeyMesh;
    [SerializeField] Mesh BallMesh;
    [SerializeField] List<Material> RageMaterials;
    [SerializeField] List<Material> NormalMaterials;
    [SerializeField] GameObject TransformationFX;
    [SerializeField] GameObject FireFX;
    [SerializeField] AudioClip RageModeSound;
    string murderBallTag = "WreckingBall";
    string normalBallTag = "Ball";
    private bool murderMode;

    float rageTime;
    [SerializeField] float RageDuration = 15f;

    float growTime;
    [SerializeField] float GrowDuration = 15f;

    private Vector3 defaultSize = new Vector3(1f, 1f, 1f);
    private Vector3 growSize = new Vector3(3f, 3f, 3f);
    private Vector3 shrinkSize = new Vector3(0.5f, 0.5f, 0.5f);
    private Vector3 targetSize;

    private void Awake()
    {
        rigidBody = GetComponent<Rigidbody>();
        var mr = GetComponent<MeshRenderer>();
       // NormalMaterials = mr.materials.ToList();
        targetSize = defaultSize;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetMurderMode()
    {
        // replace the mesh with the spikeymesh
        rageTime = Time.time;
        murderMode = true;

        var mr = gameObject.GetComponent<MeshRenderer>();
        mr.materials = RageMaterials.ToArray();
        var sphereMesh = gameObject.GetComponent<MeshFilter>();
        sphereMesh.mesh = SpikeyMesh;

        rigidBody.AddTorque(new Vector3(0f, 0f, 100f));
        
        // change the tag to be the murder tag.

        gameObject.tag = murderBallTag;
        if (TransformationFX != null)
        {
            Instantiate(TransformationFX, gameObject.transform.position, Quaternion.identity);
            AudioController.MainAudioController.PlaySound(RageModeSound);
        }

        FireFX.SetActive(true);
    }

    public void SetNormalMode()
    {
        // replace the mesh with the normal ball mesh
        murderMode = false;

        var sphereMesh = gameObject.GetComponent<MeshFilter>();
        sphereMesh.mesh = BallMesh;
        var mr = gameObject.GetComponent<MeshRenderer>();
        mr.materials = NormalMaterials.ToArray();

        // change the tag to be the murder tag.

        gameObject.tag = normalBallTag;
        FireFX.SetActive(false);
    }

    public void Grow()
    {
        growTime = Time.time;
        targetSize = growSize;
    }

    public void Shrink()
    {
        growTime = Time.time;
        targetSize = shrinkSize;
    }

    public void ReturnToNormalSize()
    {
        targetSize = defaultSize;
    }

    private float getVelocity( Vector3 v3Velocity)
    {
        var velocity = Mathf.Sqrt(( Mathf.Abs( v3Velocity.x) * Mathf.Abs(v3Velocity.x)) + (Mathf.Abs(v3Velocity.y) * Mathf.Abs(v3Velocity.y)));
        return velocity;
    }

    Vector3 currScaleVelocity;
    private void FixedUpdate()
    {
        if( Time.time > rageTime + RageDuration && murderMode)
        {
            SetNormalMode();
        }

        if(growTime > 0 && Time.time > growTime + GrowDuration)
        {
            ReturnToNormalSize();
        }

        var currentSize = gameObject.transform.localScale;
        var desiredSize = targetSize;

        var result = Vector3.SmoothDamp(currentSize, desiredSize, ref currScaleVelocity, 3f);
        gameObject.transform.localScale = result;

        if (launched)
        {
            
            var pointVelocity = rigidBody.GetPointVelocity(gameObject.transform.position);

            var currentVelocity = Mathf.Round( getVelocity(pointVelocity));

            var expectedVelocity = Mathf.Round( getVelocity(launchForce)) * ( murderMode ? 2f: 1f ) ;

            if (expectedVelocity > currentVelocity)
            {
                print(string.Format("Expected {0} got {1}", expectedVelocity, currentVelocity));
                if (expectedVelocity > Mathf.Epsilon)
                {
                    float speedDifference = expectedVelocity - currentVelocity;

                    float speedFactor = speedDifference / expectedVelocity;
                    print("Boost the ball by " + speedFactor.ToString() + "x");
                    rigidBody.AddForce(pointVelocity * speedFactor, ForceMode.VelocityChange);
                }
                else
                {
                    // WTF the ball stopped
                    print("Ball has stopped... destroy universe");
                }
            }

           // print(pointVelocity);
        }
    }

    public void RelocateBall( Vector3 newPosition, bool arrestBall )
    {
        if (arrestBall)
        {
            var rigidBody = GetComponent<Rigidbody>();
            rigidBody.velocity = Vector3.zero;
            rigidBody.angularVelocity = Vector3.zero;

            launched = false;
        }

        gameObject.transform.position = newPosition;
    }

    

    public void LaunchBall(Vector3 direction)
    {
        if (launched)
        {
            return;
        }
        var rigidBody = GetComponent<Rigidbody>();
        if (rigidBody == null)
        {
            print("Could not find rigid body");
        }
        rigidBody.AddForce(direction, ForceMode.VelocityChange);
        launchForce = direction;

        launched = true;
    }

    private void OnCollisionEnter(Collision collision)
    {

        // Add some random variance

        var contact = collision.GetContact(0);
        
        
        contactAngle = Vector3.Angle(gameObject.transform.position, collision.collider.transform.position);

        if (collision.gameObject.tag == "Bat")
        {
            //var audio = gameObject.GetComponent<AudioSource>();
            var sound = BounceSounds[Random.Range(0, BounceSounds.Count)];
            AudioController.MainAudioController.PlaySound(sound);
        }
    }
}
