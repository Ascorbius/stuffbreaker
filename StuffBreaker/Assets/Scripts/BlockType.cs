﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/BlockInfo", order = 1)]
[Serializable]
public class BlockType : ScriptableObject
{
    public string Key;
    public bool MustDestroyToComplete;
    public bool LevelTreasure;
    public float DifficultyRating;
    public GameObject Block;
}
