﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class BrickController : MonoBehaviour
{
    [SerializeField] List<AudioClip> BounceSounds;
    [SerializeField] GameObject ExplosionFX;
    [SerializeField] PowerUpOptions PowerUpDropOnDestruction;
    public LevelController daddy;
    public bool isDead = false;

    public virtual void OnCollisionEnter(Collision collision)
    {
        if (isDead)
        {
            return;
        }

        if (HitByBall(collision) || HitByLaser(collision))
        {
            DestroyBlock();
        }

        if (HitByWreckingBall(collision))
        {
            var collider = gameObject.GetComponent<Collider>();
            if (collider != null)
            {
                Physics.IgnoreCollision(collision.collider, collider);
            }
        }

    }

    public void PlayBounceSound()
    {        
        if (BounceSounds.Count > 0)
        {
            var sound = BounceSounds[Random.Range(0, BounceSounds.Count - 1)];
            print("Trying to play sound " + sound.name);
            AudioController.MainAudioController.PlaySound(sound);
        }        
    }

    public bool HitByBall( Collision collision )
    {
        return collision.gameObject.tag == "Ball" || HitByWreckingBall(collision) ;
    }

    public bool HitByWreckingBall( Collision collision)
    {
        return collision.gameObject.tag == "WreckingBall";
    }

    public bool HitByLaser( Collision collision)
    {
        return collision.gameObject.tag == "Laser";
    }

    public void DestroyBlock(  )
    {
        isDead = true;
        var fx = Instantiate(ExplosionFX, transform.position, Quaternion.identity);

        daddy.HeGotMe( gameObject.transform.position, PowerUpDropOnDestruction);

        Destroy(gameObject);        
    }
}
