﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasFader : MonoBehaviour
{
    [SerializeField] float SecondsTillFade;
    [SerializeField] float FadeSpeed;
    private CanvasGroup canvasGroup;
    private float startTime;

    // Start is called before the first frame update
    void Start()
    {
        
        Reset();
    }

    private void OnEnable()
    {
        Reset();
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.fixedTime > startTime + SecondsTillFade && canvasGroup.alpha > Mathf.Epsilon)
        {
            canvasGroup.alpha -= (FadeSpeed * Time.deltaTime);
            if(canvasGroup.alpha <= Mathf.Epsilon)
            {
                gameObject.SetActive(false);
            }
        }
    }

    public void Reset()
    {
        print("Reset was called");
        startTime = Time.fixedTime;
        canvasGroup = gameObject.GetComponent<CanvasGroup>();
        canvasGroup.alpha = 1;
    }
}
