﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableController : MonoBehaviour
{
    public LevelController daddy;
    public PowerUpOptions powerup;
    [SerializeField] AudioSource collectedSound;
    [SerializeField] GameObject collectionFX;
    [SerializeField] GameObject parent;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnCollisionEnter(Collision collision)
    {
        // If the colliding object is a bat we want to tell the game to award or punish the player in some way.

        if (collision.gameObject.tag == "Bat")
        {
            daddy.PowerupCollected(powerup);
            // Play the sound
            // Instantiate the victory thing
            Instantiate(collectionFX, collision.transform.position, Quaternion.identity);

            Destroy(gameObject);
        }

        if(collision.gameObject.tag == "Death")
        {
            Destroy(gameObject);
        }
        else
        {
            var collider = gameObject.GetComponent<Collider>();
            if (collider != null)
            {
                Physics.IgnoreCollision(collision.collider, collider);
            }
            else
            {
                print("How did this collide?");
            }
        }
    }
}
