﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuckNames 
{
    private List<string> DuckyNames;
    private int currentDuckyIndex = 0;

    public DuckNames()
    {
        DuckyNames = new List<string>();

        DuckyNames.Add("Inf Whale");
        DuckyNames.Add("Spookyalot");
        DuckyNames.Add("Hunter241");
        DuckyNames.Add("JayneGrey");
        DuckyNames.Add("RobZorz");
        DuckyNames.Add("Michi122211");
        DuckyNames.Add("Themorlock123");
        DuckyNames.Add("Kur0kenshi");
        DuckyNames.Add("StrarQuantumm");
        DuckyNames.Add("Krillakov");
        DuckyNames.Add("Jack Lyttle");
        DuckyNames.Add("Wgaffa");
        DuckyNames.Add("EpaphysUK");
        DuckyNames.Add("Turjan");
        DuckyNames.Add("Kandonian");
        DuckyNames.Add("Greytest");
        DuckyNames.Add("El Scorbius");
        DuckyNames.Add("Mach1ne");
        DuckyNames.Add("Spatula007");
        DuckyNames.Add("Tantulous");
        DuckyNames.Add("Yenots");
        DuckyNames.Add("Nuke");
        DuckyNames.Add("Lauri0s");
        DuckyNames.Add("Asteconn");
        DuckyNames.Add("Buur");
        DuckyNames.Add("Rheeney");
        DuckyNames.Add("DavidCRicardo");
        DuckyNames.Add("ShivEmUp");
        DuckyNames.Add("Jeremiahfieldhaven");
        DuckyNames.Add("Nyx1962");
        DuckyNames.Add("m3xpl4y");
        DuckyNames.Add("its_micro_t");
        DuckyNames.Add("Lakea Gaming");

    }

    public string GetDuckName()
    {
        if(currentDuckyIndex >= DuckyNames.Count)
        {
            currentDuckyIndex = 0;
        }
        return DuckyNames[currentDuckyIndex++];
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
