﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Cameras;

public class DuckyController : MonoBehaviour
{
    [SerializeField] List<AudioClip> QuacksSounds;
    [SerializeField] AudioClip DeathSound;
    [SerializeField] GameObject DeathFX;
    private bool isDead = false;
    public LevelController LevelController;
    [SerializeField] TextMesh textMesh;
    [SerializeField] GameObject aligner;
    public GameObject lookAt;

    public string DuckyName;

    private float commandTime;
    private float secondsTillNextCommand;

    private void Awake()
    {
    }

    public void SetDuckyAlignment( GameObject gameObject)
    {
        var lookAtTarget = aligner.GetComponent<LookatTarget>();
        lookAt = gameObject;
        lookAtTarget.SetTarget(lookAt.transform);
    }

    private void refreshLookat()
    {
        var lookAtTarget = aligner.GetComponent<LookatTarget>();
        lookAtTarget.SetTarget(lookAt.transform);
    }

    public void SetDuckyName( string name)
    {
        DuckyName = name;
        textMesh.text = DuckyName;
    }

    // Start is called before the first frame update
    void Start()
    {
        ResetCommandTime();
    }

    void ResetCommandTime()
    {
        commandTime = Time.time; // Seconds since start of game.
        secondsTillNextCommand = Random.Range(1, 30);
    }

    // Update is called once per frame
    void Update()
    {
        refreshLookat();
        if ( Time.time > commandTime + secondsTillNextCommand)
        {
            if (QuacksSounds.Count > 0)
            {
                var sound = QuacksSounds[Random.Range(0, QuacksSounds.Count - 1)];
                print("Trying to play quack sound " + sound.name);
                AudioController.MainAudioController.PlaySound(sound);
            }

            var rigidBody = gameObject.GetComponent<Rigidbody>();
            gameObject.transform.Rotate(new Vector3(0f, Random.Range(0, 360), 0f));
            //rigidBody.AddTorque(new Vector3(0f, Random.Range(0, 360), 0f));
            rigidBody.AddForce(transform.forward * 50 );
            ResetCommandTime();
        }
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (isDead)
        {
            return;
        }
        if (collision.gameObject.tag == "Ball" || collision.gameObject.tag == "WreckingBall" )
        {
            isDead = true;
            print("Ducky Boop Ball");
            DestroyDucky();
        }
    }

    public void DestroyDucky()
    {
        var fx = Instantiate(DeathFX, transform.position, Quaternion.identity);

        if (DeathSound != null)
        {
            AudioController.MainAudioController.PlaySound(DeathSound);
        }

        LevelController.DuckyDead();

        Destroy(gameObject);
    }
}
