﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMetaData
{
    public Dictionary<string, BlockType> BlockTypes;
    public Dictionary<string, PowerupType> Powerups;

    public GameMetaData(Dictionary<string, BlockType> blockTypes, Dictionary<string, PowerupType> powerupTypes)
    {
        BlockTypes = blockTypes;
        Powerups = powerupTypes;
    }
}
