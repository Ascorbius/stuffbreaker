﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Security.Cryptography;
using Newtonsoft.Json;
using System.IO.IsolatedStorage;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

[System.Serializable]
public class SavePayload
{
    public System.DateTime FileSaveDate;
    [SerializeField] public List<LevelStatistics> LevelStatistics;
}

public class GameStateController : MonoBehaviour
{
    private static GameStateController _instance;

    List<LevelData> levels;
    Dictionary<string, LevelStatistics> levelsStatistics;
    
    [SerializeField] int levelIndex = 0;
    [SerializeField] int levelCount = 0;
    [SerializeField] List<BlockType> BlockTypes;
    private LevelData currentLevel = null;

    private string gameKey = "35093891678349284753987234250349579871234";
    private string levelSignKey = "345308518294987239873985793459877277277277";

    public static GameStateController MainGameStateController { get { return _instance; }}


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);

        }
        print("Loading levels");
        loadLevels();
        print("Loading previous state");
        LoadState();
        print("Setting current level");
        setCurrentLevel(_instance.levelIndex);

    }

    private void Start()
    {
        print("GameStateController started");
        // Load the level statistics
        
    }

    private Dictionary<string, BlockType> GetBlockTypeList()
    {
        var results = new Dictionary<string, BlockType>();
        foreach ( var block in BlockTypes)
        {
            if (!results.ContainsKey(block.Key))
            {
                results.Add(block.Key, block);
            }
        }
        return results;
    }

    private void loadLevels()
    {

        var metaData = new GameMetaData( GetBlockTypeList(), null );

        print("GameStateController loadLevels");
        // Load all of the levels from the levels resource folder.
        //var levelFiles = Resources.LoadAll<TextAsset>("Levels");

        var levelFiles = new List<string>();
        foreach( var filename in Directory.EnumerateFiles( Path.Combine( Application.streamingAssetsPath, "Levels")))
        {
            var file = File.ReadAllText(filename);
            levelFiles.Add(file);
        }



        var tempLevelList = new List<LevelData>();

        foreach (var levelFile in levelFiles)
        {
            try
            {
                var levelData = JsonUtility.FromJson<LevelData>(levelFile);
                levelData.CalculateDifficulty(metaData);
                var levelHash = GetHash(levelFile, gameKey);
                levelData.LevelHash = levelHash;
                tempLevelList.Add(levelData);
            }
            catch( System.Exception ex)
            {
                print("Unable to load level");
            }
        }

        _instance.levels = tempLevelList.OrderBy( x => x.DifficultRating ).ToList();
        _instance.levelCount = _instance.levels.Count;

       
    }

    public void setCurrentLevel( int levelId)
    {
        print("GameStateController setCurrentLevel with " + levelId);
        _instance.currentLevel = _instance.levels[levelId];
        _instance.levelIndex = levelId;
    }

   
    public LevelData GetCurrentLevel()
    {
        print("GameStateController GetCurrentLevel");
        print("GameStateController GetCurrentLevel " + _instance.levels.Count + " levels");
        if (_instance.currentLevel == null)
        {
            print("GameStateController currentLevel was null... why?");
        }
        return _instance.currentLevel;
    }

    public LevelData GetNextLevel()
    {
        print("GameStateController GetNextLevel");
        print("Previous level was " + _instance.levelIndex);
        _instance.levelIndex++;
        print("New level is " + _instance.levelIndex);
        setCurrentLevel(_instance.levelIndex);
        return GetCurrentLevel();

    }

    public void SetLevelStatistics( LevelStatistics playerLevelStats)
    {
        //TODO: Persist this for the level select screen.
        if(_instance.levelsStatistics.ContainsKey(playerLevelStats.LevelHash))
        {
            var currentScore = _instance.levelsStatistics[playerLevelStats.LevelHash];
            _instance.levelsStatistics.Remove(playerLevelStats.LevelHash);

            if (playerLevelStats.Score < currentScore.Score)
            {
                playerLevelStats.Score = currentScore.Score;
            }
            if (playerLevelStats.DuckiesMurdered > currentScore.DuckiesMurdered)
            {
                playerLevelStats.DuckiesMurdered = currentScore.DuckiesMurdered;
            }
            if (playerLevelStats.TreasureStar < currentScore.TreasureStar)
            {
                playerLevelStats.TreasureStar = currentScore.TreasureStar;
            }
            if (playerLevelStats.DuckyStar < currentScore.DuckyStar)
            {
                playerLevelStats.DuckyStar = currentScore.DuckyStar;
            }
            if (playerLevelStats.ComboStar < currentScore.ComboStar)
            {
                playerLevelStats.ComboStar = currentScore.ComboStar;
            }
        }

        

        _instance.levelsStatistics.Add(playerLevelStats.LevelHash, playerLevelStats);
        var json = JsonUtility.ToJson(playerLevelStats);
        print(json);
        //TODO: Save this somehow
        saveState();
    }

    private string getFileName()
    {
        string[] pathParts = new string[] { Application.persistentDataPath, "saves.dat" };
        return Path.Combine(pathParts);

    }

    private void saveState()
    {
        // save the current level statistics
        var saveMe = new SavePayload();
        saveMe.FileSaveDate = System.DateTime.Now;

        var statList = new List<LevelStatistics>();

        foreach ( var stat in _instance.levelsStatistics)
        {
            statList.Add(stat.Value);
        }
        saveMe.LevelStatistics = statList;


        var payloadJson = JsonConvert.SerializeObject(saveMe);

        var tamperText = SignText(payloadJson);

        System.IO.File.WriteAllText(getFileName(), tamperText);

    }

    public string SignText( string jsonIn)
    {
        // Take Json in and sign it

        var hash = GetHash(jsonIn, levelSignKey);
        jsonIn += "----Signature----" + hash;
        // append signature to the bottom
        return jsonIn;
    }

    public string TamperTest( string signedJsonIn)
    {
        // Extract signature
        var originalJson = signedJsonIn.Substring(0, signedJsonIn.IndexOf("----Signature----"));
        // sign remainder
        var fileHash = GetHash(originalJson, levelSignKey);
        // compare signature
        var compareHash = signedJsonIn.Substring(originalJson.Length + "----Signature----".Length);
        // if match, return unsigned remainder
        if(compareHash == fileHash)
        {
            return originalJson;
        }
        // If not match, return empty text;

        return "";
    }

    private void LoadState()
    {
        // Load the save file.
        // Deserialize to object
        // Replace level statistics.

        if (File.Exists(getFileName()))
        {
            var text = File.ReadAllText(getFileName());
            var validatedText = TamperTest(text);
            var restoredPayload = JsonConvert.DeserializeObject<SavePayload>(validatedText);
            if (_instance.levelsStatistics == null)
            {
                _instance.levelsStatistics = new Dictionary<string, LevelStatistics>();
            }
            else
            {
                _instance.levelsStatistics.Clear();
            }
            foreach (var stat in restoredPayload.LevelStatistics)
            {
                _instance.levelsStatistics.Add(stat.LevelHash, stat);
            }

        }
        else
        {
            _instance.levelsStatistics = new Dictionary<string, LevelStatistics>();
        }
        
    }


    public LevelStatistics GetLevelStatistics( string levelHash )
    {
        if (_instance.levelsStatistics.ContainsKey(levelHash))
        {
            return _instance.levelsStatistics[levelHash];
        }
        return null;
    }

    public List<LevelData> GetLevels()
    {
        return _instance.levels;
    }

    private string GetHash(string text, string key)
    {
        System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();

        byte[] textBytes = encoding.GetBytes(text);
        byte[] keyBytes = encoding.GetBytes(key);

        byte[] hashBytes;

        using (HMACSHA1 hash = new HMACSHA1(keyBytes))
            hashBytes = hash.ComputeHash(textBytes);

        return System.BitConverter.ToString(hashBytes).Replace("-", "").ToLower();
    }

    private string Base64Encode(string plainText)
    {
        var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
        return System.Convert.ToBase64String(plainTextBytes);
    }


}
