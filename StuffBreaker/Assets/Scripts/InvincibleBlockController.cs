﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvincibleBlockController : BrickController
{

    public override void OnCollisionEnter(Collision collision)
    {
        if (!HitByBall(collision))
        {
            return;
        }

        if (HitByWreckingBall(collision))
        {
            base.OnCollisionEnter(collision);
        }

        PlayBounceSound();
        
    }
}
