﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBoltController : MonoBehaviour
{
    private Rigidbody rigidBody;
    private bool launched = false;
    [SerializeField] GameObject LaserExplosionFX;

    private void Awake()
    {
        rigidBody = GetComponent<Rigidbody>();
    }

    public void FireLaser( Vector3 launchForce)
    {
        if (launched)
        {
            return;
        }
        rigidBody.AddForce(launchForce, ForceMode.VelocityChange);
        launched = true;
    }

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag != "Bat" && collision.gameObject.tag != "Laser" )
        {
            Instantiate(LaserExplosionFX, gameObject.transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
