﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBehaviour : MonoBehaviour
{
    [SerializeField] public float RotationSpeed = 0;
    [SerializeField] public Vector3 MovementVector = new Vector3(0f, 0f, 0f);
    [SerializeField] public float MovementPeriod = 2f;

    private Vector3 startingPosition;
    float movementFactor; // 0 not moved, 1 fully moved

    // Start is called before the first frame update
    void Start()
    {
        startingPosition = transform.position;
    }

    public void Reset()
    {
        transform.position = startingPosition;
        transform.Rotate( new Vector3(0, 0, 0));
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.forward * (RotationSpeed * Time.deltaTime));
        if (MovementPeriod <= Mathf.Epsilon)
        {
            return;
        }

        float cycles = Time.time / MovementPeriod;

        const float tau = Mathf.PI * 2;
        float rawSineWave = Mathf.Sin(cycles * tau);

        movementFactor = rawSineWave / 2f + 0.5f;

        Vector3 offset = MovementVector * movementFactor;
        transform.position = startingPosition + offset;

    }
}
