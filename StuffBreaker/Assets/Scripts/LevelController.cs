﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelController : MonoBehaviour
{
    [SerializeField] List<BlockType> blocks;
    [SerializeField] List<PowerupType> powerups;

    [SerializeField] GameObject parentContainer;

    [SerializeField] GameObject PlayerBat;

    [SerializeField] float LevelXOffset = 0;
    [SerializeField] float LevelYOffset = 0;
    [SerializeField] float BlockWidth = 2;
    [SerializeField] float BlockHeight = 1;

    [SerializeField] int MaximumBlocks = 8;

    [SerializeField] GameObject MultiBallPrefab;

    [SerializeField] GameObject DuckyParent;
    [SerializeField] GameObject DuckyPrefab;
    [SerializeField] float MinDuckyX = -14f;
    [SerializeField] float MaxDuckyX = 14f;
    [SerializeField] float DuckyY = -5.5f;
    [Tooltip("How many duckies shall we start the game with?")][SerializeField] int InitialDuckyCount = 10;
    [Tooltip("How many of the remaining duckies shall we award based on existing duckies")][SerializeField] float CompletionDuckyAward = 0.5f;
    [Tooltip("No matter what, you'll get this many duckies at the end of the level")][SerializeField] int MinDuckyAward = 1;

    [SerializeField] Text LevelAuthor;
    [SerializeField] Text LevelName;

    [SerializeField] GameObject GameOverCanvas;
    [SerializeField] GameObject VictoryCanvas;
    [SerializeField] GameObject LevelCanvas;
    [SerializeField] GameObject StarManager;
    [SerializeField] GameObject Camera;

    [SerializeField] Text ScoreText;
    [SerializeField] Text ComboText;
    [SerializeField] int ScorePerBlock = 5;
    [SerializeField] int maxCombo = 10;
    [SerializeField] int ScorePerGoldCoin = 100;
    private int highCombo = 0;

    private int currentCombo = 0;
    private int currentScore = 0;
    private int duckMurderCount = 0;

    private float levelStartTime = 0;

    private int currentTreasureCount = 0;
    private int levelTreasureCount = 0;

    private DuckNames duckNameGen;

    [SerializeField] int NoStarMinCombo = 0;
    [SerializeField] int NoStarMaxCombo = 1;
    [SerializeField] int BronzeStarMinCombo = 2;
    [SerializeField] int BronzeStarMaxCombo = 4;
    [SerializeField] int SilverStarMinCombo = 5;
    [SerializeField] int SilverStarMaxCombo = 8;
    [SerializeField] int GoldStarMinCombo = 9;
    [SerializeField] int GoldStarMaxCombo = 10;

    [SerializeField] int NoStarMinDuckies = 0;
    [SerializeField] int NoStarMaxDuckies = 25;
    [SerializeField] int BronzeStarMinDuckies = 26;
    [SerializeField] int BronzeStarMaxDuckies = 50;
    [SerializeField] int SilverStarMinDuckies = 51;
    [SerializeField] int SilverStarMaxDuckies = 75;
    [SerializeField] int GoldStarMinDuckies = 76;
    [SerializeField] int GoldStarMaxDuckies = 100;


    private Vector3 startingPosition;
    private float defaultLevelXOffset = 0;
    private float defaultLevelYOffset = 0;
    private float defaultBlockWidth = 2;
    private float defaultBlockHeight = 1;

    private int blocksToKill = 0;
    private int DuckyCount = 0;
    private int startingDuckyCount = 0;

    
    private LevelData currentLevel;

    private void Awake()
    {
        duckNameGen = new DuckNames();
    }

    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 60;

        defaultBlockHeight = BlockHeight;
        defaultBlockWidth = BlockWidth;
        defaultLevelXOffset = LevelXOffset;
        defaultLevelYOffset = LevelYOffset;

        startingPosition = parentContainer.transform.position;

        var gameStateController = GameStateController.MainGameStateController;

        print("Starting Game");
        currentLevel = gameStateController.GetCurrentLevel();
        print("Level is " + currentLevel.LevelName);
        if (currentLevel == null)
        {
            print("state controller returned null");
        }

        buildLevel();
        buildDuckies(InitialDuckyCount);
        startingDuckyCount = DuckyCount;
        StartLevel();

    }

    private StarColour GetComboStar()
    {
        if (highCombo >= NoStarMinCombo && highCombo <= NoStarMaxCombo)
        {
            return StarColour.None;
        }
        if (highCombo >= BronzeStarMinCombo && highCombo <= BronzeStarMaxCombo)
        {
            return StarColour.Bronze;
        }
        if (highCombo >= SilverStarMinCombo && highCombo <= SilverStarMaxCombo)
        {
            return StarColour.Silver;
        }
        if (highCombo >= GoldStarMinCombo && highCombo <= GoldStarMaxCombo)
        {
            return StarColour.Gold;
        }
        return StarColour.None;
    }

    private StarColour GetDuckyStar()
    {
        var duckyPercentage = Mathf.RoundToInt(  ((float) DuckyCount / (float) startingDuckyCount) * 100f );

        if (duckyPercentage >= NoStarMinDuckies && duckyPercentage <= NoStarMaxDuckies)
        {
            return StarColour.None;
        }
        if (duckyPercentage >= BronzeStarMinDuckies && duckyPercentage <= BronzeStarMaxDuckies)
        {
            return StarColour.Bronze;
        }
        if (duckyPercentage >= SilverStarMinDuckies && duckyPercentage <= SilverStarMaxDuckies)
        {
            return StarColour.Silver;
        }
        if (duckyPercentage >= GoldStarMinDuckies )
        {
            return StarColour.Gold;
        }
        return StarColour.None;

    }

    private StarColour GetTreasureStar()
    {
        if (levelTreasureCount > 0)
        {
            var treasurePercentage = Mathf.RoundToInt(((float)currentTreasureCount / (float)levelTreasureCount) * 100);

            if (treasurePercentage > 0 && treasurePercentage <= 25)
            {
                return StarColour.None;
            }
            if (treasurePercentage > 25 && treasurePercentage <= 50)
            {
                return StarColour.Bronze;
            }
            if (treasurePercentage > 50 && treasurePercentage <= 75)
            {
                return StarColour.Silver;
            }
            if (treasurePercentage > 75)
            {
                return StarColour.Gold;
            }
        }
        return StarColour.None;

    }



    private void resetScoring()
    {
        currentCombo = 0;
        currentScore = 0;
        highCombo = 0;
        currentTreasureCount = 0;
        duckMurderCount = 0;
    }

    private void resetCombo()
    {
        // Aw shucks, no more combo
        currentCombo = 0;
    }

    private void increaseCombo()
    {
        currentCombo++;
        if(currentCombo> maxCombo)
        {
            currentCombo = maxCombo;
        }
        if(currentCombo > highCombo)
        {
            highCombo = currentCombo;
        }
    }

    private void increaseScore()
    {
        // combo is increased on block destruction.
        currentScore += (ScorePerBlock * currentCombo);
    }

    private void increaseScoreForGoldCoin()
    {
        currentScore += (ScorePerGoldCoin * currentCombo);
    }

    private void refreshUI()
    {
        ComboText.text = currentCombo.ToString();
        ScoreText.text = currentScore.ToString();
    }


    private void StartLevel()
    {
        print("Level Start called Current level is " + currentLevel.LevelName);
        LevelName.text = currentLevel.LevelName;
        LevelAuthor.text = currentLevel.LevelAuthor;
        resetLasers();
        resetBatAndBall();
        resetScoring();
        SetPausedState(false);
        LevelCanvas.SetActive(true);

        levelStartTime = Time.time;

    }

    public void StartNextLevel()
    {
        VictoryCanvas.SetActive(false);
        SetPausedState(false);

        var gameStateController = GameStateController.MainGameStateController;
        currentLevel = gameStateController.GetNextLevel();

        var bonusDuckies = (int)Mathf.Round(Mathf.Clamp(DuckyCount * CompletionDuckyAward, MinDuckyAward, InitialDuckyCount));

        buildLevel();
        buildDuckies(bonusDuckies);
        startingDuckyCount = DuckyCount;
        StartLevel();

    }

    private void resetLasers()
    {
        var bolts = GameObject.FindGameObjectsWithTag("Laser");
        foreach(var bolt in bolts)
        {
            GameObject.Destroy(bolt);
        }
    }

    private void resetBatAndBall()
    {
        var playerController = PlayerBat.GetComponent<PlayerController>();
        playerController.ResetBatAndBall();



        var balls = GameObject.FindGameObjectsWithTag("WreckingBall");
        foreach( var ball in balls)
        {
            var bc = ball.GetComponent<BallController>();
            bc.SetNormalMode();
        }


    }

    private void createPowerup( Vector3 position, PowerUpOptions powerUp)
    {
        if (powerUp != PowerUpOptions.None)
        {
            foreach (var po in powerups)
            {
                if (po.powerupKey == powerUp)
                {
                    var powerUpObject = Instantiate(po.powerupPrefab, position, Quaternion.identity, parentContainer.transform);
                    var collectable = powerUpObject.GetComponent<CollectableController>();
                    collectable.daddy = this;
                }
            }
        }
        
    }

    public void PowerupCollected( PowerUpOptions powerUp)
    {
        // Do things based on the powerup which was collected.
        print(">>>>>>Powerup was collected " + powerUp);

        switch( powerUp)
        {
            case PowerUpOptions.GoldCoin:
                // Add to the score
                increaseScoreForGoldCoin();
                break;
            case PowerUpOptions.Treasure:
                // Add to the egg count
                currentTreasureCount++;
                break;
            case PowerUpOptions.BatLarge:
                // Tell the bat to go big.
                EnlargeBat();
                break;
            case PowerUpOptions.BatSmall:
                // Tell the bat to be smol
                ShrinkBat();
                
                break;

            case PowerUpOptions.GrowBall:
                // Tell the ball to be big
                GrowBall();
                break;
            case PowerUpOptions.ShrinkBall:
                // Tell the ball to be smol
                ShrinkBall();
                break;
            case PowerUpOptions.RageBall:
                RagingBalls();
                break;

            case PowerUpOptions.BatCannon:
                // Tell the bat to get the guns out.
                AttachGun();
                break;
            case PowerUpOptions.FreeDuckie:
                // Add a duckie
                CreateDuckie();
                break;
            case PowerUpOptions.BatShield:
                ActivateShield();
                break;

            default:
                // do nothing
                break;

        }

        void GrowBall()
        {
            // Tell all balls to be big.
            var balls = GameObject.FindGameObjectsWithTag("Ball");

            GrowBalls(balls);
            var murderBalls = GameObject.FindGameObjectsWithTag("WreckingBall");
            GrowBalls(murderBalls);
        }

        void GrowBalls( GameObject[] balls)
        {
            foreach (var ball in balls)
            {
                var bc = ball.GetComponent<BallController>();
                bc.Grow();
            };
        }

        void ShrinkBall()
        {
            // Tell all balls to be smol
            
            var balls = GameObject.FindGameObjectsWithTag("Ball");

            ShrinkBalls(balls);
            var murderBalls = GameObject.FindGameObjectsWithTag("WreckingBall");
            ShrinkBalls(murderBalls);
        }

        void ShrinkBalls(GameObject[] balls)
        {
            foreach (var ball in balls)
            {
                var bc = ball.GetComponent<BallController>();
                bc.Shrink();
            };
        }

        void EnlargeBat()
        {
            // Get the bat, Tell it to be big
            var playerController = PlayerBat.GetComponent<PlayerController>();
            playerController.EnlargeBat();
        }

        void ShrinkBat()
        {
            // Get the bat, shrink it
            var playerController = PlayerBat.GetComponent<PlayerController>();
            playerController.ShrinkBat();
        }

        void RagingBalls()
        {
            var balls = GameObject.FindGameObjectsWithTag("Ball");

            foreach( var ball in balls)
            { 
                var bc = ball.GetComponent<BallController>();
                bc.SetMurderMode();                
            }
        }

        void AttachGun()
        {
            // Tell the bat to activate the gun
            var playerController = PlayerBat.GetComponent<PlayerController>();
            playerController.AttachGun();
        }

        void ActivateShield()
        {
            // Tell the bat to activate the shields.
            var playerController = PlayerBat.GetComponent<PlayerController>();
            playerController.ActivateShield();
        }

        void CreateDuckie()
        {
            // Add a duckie to the level at a random place or perhaps at the bat location.
            MakeDucky();
        }
    }


    public void HeGotMe( Vector3 position, PowerUpOptions powerup )
    {
        blocksToKill--;
        increaseCombo();
        increaseScore();

        createPowerup( position, powerup );

        if (blocksToKill <= 0)
        {
            // Start the victory process.
            Victory();
        }
    }

    public void DuckyDead()
    {
        DuckyCount--;
        resetCombo();

        duckMurderCount++;

        if (DuckyCount <= 0)
        {
            print("All Duckies dead");
            GameOver();
        }
    }

    public void Splosh()
    {
        // ball hit water.
        resetCombo();
    }

    private void buildLevel()
    {
        blocksToKill = 0;
        levelTreasureCount = 0;

        foreach(Transform child in parentContainer.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        // Set up the level data.
        parentContainer.transform.position = startingPosition + currentLevel.PositionOffset;
        LevelXOffset = currentLevel.LevelXOffset;// <= Mathf.Epsilon ? defaultLevelXOffset: level.LevelXOffset;
        LevelYOffset = currentLevel.LevelYOffset;// <= Mathf.Epsilon ? defaultLevelYOffset: level.LevelYOffset;
        BlockWidth = currentLevel.BlockWidth <= Mathf.Epsilon ? defaultBlockWidth: currentLevel.BlockWidth;
        BlockHeight = currentLevel.BlockWidth <= Mathf.Epsilon ? defaultBlockHeight : currentLevel.BlockHeight;
        parentContainer.transform.localScale = new Vector3(1f, 1f, 1f);

        var levelBehaviour = parentContainer.GetComponent<LevelBehaviour>();
        levelBehaviour.RotationSpeed = currentLevel.RotationSpeed;
        levelBehaviour.MovementVector = currentLevel.MovementVector;
        levelBehaviour.MovementPeriod = currentLevel.MovementPeriod;


        var rowCount = currentLevel.LevelHeight();
        var colCount = currentLevel.LevelWidth();

        var blockIndex = new Dictionary<string, int>();
        int index = 0;
        foreach( var block in blocks)
        {
            try
            {
                blockIndex.Add(block.Key, index);
                index++;
            }
            catch(System.Exception ex)
            {
                print("Oops");
            }
        }

        var levelColumns = currentLevel.LevelWidth();
        var levelRows = currentLevel.LevelHeight();

        var realBlockWidth = BlockWidth / 2;

        for (int y = 0; y < rowCount ; y++)
        {
            for( int x = 0; x< colCount ; x++)
            {
                var arrayBlock = currentLevel.GetData(x,y);

                if (  arrayBlock != "" && arrayBlock != " " )
                {
                    BlockType block = blocks[0]; ;
                    if (blockIndex.ContainsKey(arrayBlock))
                    {
                        block = blocks[blockIndex[arrayBlock]];
                    }


                    float xPos = transform.position.x - ( ((levelColumns * BlockWidth) /2) + LevelXOffset) + (x * BlockWidth);
                    float yPos = transform.position.y + ( ((levelRows * BlockHeight) /2) + LevelYOffset) - (y * BlockHeight);

                    var newBlock = Instantiate(block.Block, new Vector3(xPos, yPos, 0f), Quaternion.identity);
                    newBlock.transform.localScale = new Vector3(realBlockWidth, BlockHeight, 1f);
                    var blockController = newBlock.GetComponent<BrickController>();
                    blockController.daddy = this;

                    newBlock.transform.parent = parentContainer.transform;

                    if (block.MustDestroyToComplete)
                    {
                        blocksToKill++;
                    }
                    if (block.LevelTreasure)
                    {
                        levelTreasureCount++;
                    }
                }

            }
        }

        

        
        print("level:" + levelColumns.ToString() + " Max:" + MaximumBlocks.ToString());
        if (levelColumns > MaximumBlocks && MaximumBlocks > 0)
        {
            print("Level too wide");
            float scaleFactor = (float)MaximumBlocks / (float)levelColumns;
            print("Scale factor " + scaleFactor.ToString());
            parentContainer.transform.localScale = new Vector3(scaleFactor, scaleFactor, 1f);
        }
        else
        {
            
        }
    }

    private void buildDuckies( int duckCount)
    {
        for( var d = 0; d< duckCount; d++)
        {
            MakeDucky();
        }
    }

    private void MakeDucky()
    {
        
        var duckyX = Random.Range(MinDuckyX, MaxDuckyX);

        MakeDuckyAt(duckyX, DuckyY);
    }

    private void MakeDuckyAt( float duckX, float duckY)
    {
        print("Making Ducky at " + duckX + "," + duckY);
        var newDuck = Instantiate(DuckyPrefab, new Vector3(duckX, duckY, 0f), Quaternion.identity);
        var duckController = newDuck.GetComponent<DuckyController>();
        duckController.LevelController = this;
        duckController.SetDuckyAlignment(Camera);
        duckController.SetDuckyName(duckNameGen.GetDuckName());
        DuckyCount++;
        newDuck.transform.parent = DuckyParent.transform;
    }

    private void GameOver()
    {
        // Pause the game
        SetPausedState(true);
        // Show the Gameover UI
        GameOverCanvas.SetActive(true);

    }

    private void Victory()
    {
        // Pause the game
        SetPausedState(true);

        // Gather statistics
        // Set the Victory stats

        // Combo Stats
        var comboColour = GetComboStar();

        // Ducky Stats
        var duckyColour = GetDuckyStar();

        // Object in level stats.
        var treasureColour = GetTreasureStar();

        var starManagerController = StarManager.GetComponent<StarManagerController>();
        

        // Show the Victory UI
        VictoryCanvas.SetActive(true);
        starManagerController.StartStars(comboColour, duckyColour, treasureColour);

        var statsForLevel = new LevelStatistics()
        {
            BallMisses = 0,
            BulletsFired = 0,
            DuckiesMurdered = duckMurderCount,
            Score = currentScore,
            LevelHash = currentLevel.LevelHash,
            TimeTaken = Time.time - levelStartTime,
            DuckyStar = duckyColour,
            ComboStar = comboColour,
            TreasureStar = treasureColour
        };

        GameStateController.MainGameStateController.SetLevelStatistics(statsForLevel);

    }

    public void RestartGame()
    {
        buildLevel();
        buildDuckies(startingDuckyCount); // Set the duckies to that which the player started the level with. No new duckies.
        StartLevel();
        GameOverCanvas.SetActive(false);
    }


    // Update is called once per frame
    void Update()
    {
        refreshUI();
    }

    private void SetPausedState(bool pausedState)
    {
        Time.timeScale = pausedState ? 0 : 1;
    }
}
