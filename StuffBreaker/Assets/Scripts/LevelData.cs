﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

public class LevelData
{
    public string LevelHash = "";
    public string LevelName = "";
    public string LevelAuthor = "";
    public List<string> Rows;
    public float RotationSpeed;
    public Vector3 PositionOffset = new Vector3(0f, 0f, 0f);
    public float LevelXOffset = 0f;
    public float LevelYOffset = 0f;
    public float BlockWidth = 2f;
    public float BlockHeight = 1f;
    public Vector3 MovementVector = new Vector3(0f, 0f, 0f);
    public float MovementPeriod = 0f;
    public int DifficultRating = 0;
    // Offset from the auto calculated difficulty to give an organic progression

    public int LevelOffset = 0;

    public LevelData()
    {
        Rows = new List<string>();
    }

    public void AddRow( string rowData)
    {
        Rows.Add(rowData);
    }

    public string GetData( int x, int y)
    {
        if( y < Rows.Count && y >= 0)
        {
            var row = Rows[y];
            if( x < row.Length && x >= 0)
            {
                var letter = row.Substring(x,1);
                return letter;
            }
        }

        return "";
    }

    public int LevelHeight()
    {
        return Rows.Count;
    }

    public int LevelWidth()
    {
        int maxLength = 0;
        foreach ( var row in Rows)
        {
            if(maxLength < row.Length)
            {
                maxLength = row.Length;
            }
        }
        return maxLength;
    }

    public void CalculateDifficulty( GameMetaData metaData  )
    {
        float levelScore = 0;
        var brickCount = 0;

        var rows = LevelHeight();
        var cols = LevelWidth();

        for(int x = 0; x< cols; x++)
        {
            for(int y = 0; y< rows; y++)
            {
                var blockCode = GetData(x, y);
                if (metaData.BlockTypes.ContainsKey(blockCode))
                {
                    var blockData = metaData.BlockTypes[blockCode];
                    levelScore += blockData.DifficultyRating;
                    brickCount++;
                }
            }
        }

        DifficultRating = Mathf.RoundToInt( levelScore ) + LevelOffset;
    }

}
