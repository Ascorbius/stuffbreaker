﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelStatistics 
{
    public string LevelHash;
    public int DuckiesMurdered;
    public int BulletsFired;
    public int BallMisses;
    public float TimeTaken;


    public StarColour DuckyStar;
    public StarColour ComboStar;
    public StarColour TreasureStar;
    public int Score;

}
