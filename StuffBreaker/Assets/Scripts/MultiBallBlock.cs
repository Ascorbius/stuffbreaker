﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiBallBlock : ToughBlockController
{
    [Header("Ball settings")]
    [SerializeField] GameObject multiHeroBallPrefab;
    [SerializeField] Vector2 LaunchMinRange = new Vector2(-10f,-10f);
    [SerializeField] Vector2 LaunchMaxRange = new Vector2(10f,10f);


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void OnCollisionEnter(Collision collision)
    {
        base.OnCollisionEnter(collision);
        if (IsDead())
        {
            var ball = Instantiate(multiHeroBallPrefab, collision.transform.position, Quaternion.identity, gameObject.transform.parent);
            var ballControl = ball.GetComponent<BallController>();
            ballControl.LaunchBall(new Vector3( Random.Range( LaunchMinRange.x,LaunchMaxRange.x ), Random.Range(LaunchMinRange.y, LaunchMaxRange.y),0f  ));
        }
    }
}
