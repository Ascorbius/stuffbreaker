﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PauseMenuController : MonoBehaviour
{
    private bool paused = false;
    [SerializeField] GameObject pauseCanvas;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var pausedPressed = CrossPlatformInputManager.GetButtonDown("Option");
        if (pausedPressed)
        {
            paused = !paused;

            SetPausedState(paused);
            
        }
        
    }

    private void SetPausedState( bool pausedState)
    {
        pauseCanvas.SetActive(pausedState);
        Time.timeScale = paused ? 0 : 1;
    }

    public void ResumeGame()
    {
        paused = false;
        SetPausedState(paused);
    }
}
