﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour
{
    [SerializeField] float MinPosition = -3f;
    [SerializeField] float MaxPosition = 3f;
    [SerializeField] float BatSpeed = 3f;

    [SerializeField] float MinFireAngle = -45f;
    [SerializeField] float MaxFireAngle = 45f;
    [SerializeField] float LaunchForce = 200f;
    [SerializeField] int Ammo = 20;

    [SerializeField] GameObject ball = null;
    [SerializeField] GameObject ballPrefab;
    [SerializeField] GameObject BatBase;
    [SerializeField] Vector3 BatStartPosition;
    [SerializeField] Vector3 BallStartPosition;
    [SerializeField] GameObject FakeBall;
    [SerializeField] GameObject LeftSheild;
    [SerializeField] GameObject RightShield;
    [SerializeField] GameObject Gun;
    [SerializeField] GameObject BulletPrefab;
    [SerializeField] Vector3 BulletSpeed = new Vector3(0f, 20f, 0f);
    [SerializeField] AudioClip FireLaserSound;

    [SerializeField] Vector3 BulletSpawnPoint;

    [SerializeField] float BatSizeStateTime = 30f;
    [SerializeField] float BatResizeTime = 3f;
    [SerializeField] float ShieldActiveTime = 20f;
    [SerializeField] AudioClip BatExpandClip;
    [SerializeField] AudioClip BatReturnClip;
    [SerializeField] AudioClip BatShrinkClip;
    [SerializeField] AudioClip ShieldSoundOnClip;
    [SerializeField] AudioClip ShieldSoundOffClip;
    [SerializeField] AudioClip ShieldLoopClip;
    [SerializeField] float ShieldLoopSoundDelay = .3f;

    private int currentAmmo = 0;

    [SerializeField] float BatSize = 2f;
    private float TargetBatSize = 2f;

    private float BatSizeTime = 0;
    private bool isResizing = false;
    private float ShieldTime = 0;
    private bool isShieldActive = false;




    private void Awake()
    {
        
    }
    // Start is called before the first frame update
    void Start()
    {
        BatStartPosition = gameObject.transform.position;
        BallStartPosition = ball.transform.position;
        ShowFakeBall();
        ResetBat();
    }

    // Update is called once per frame
    void Update()
    {
        processTranslation();
        processBallLaunch();

        processShieldLife();
        processBatSize();
    }

    void processShieldLife()
    {
        if (isShieldActive)
        {
            if(Time.time > ShieldTime + ShieldActiveTime)
            {
                deactivateShield();
            }
        }
    }


    Vector3 currVelocity;
    void processBatSize()
    {
        if( BatSizeTime + BatSizeStateTime < Time.time)
        {
            // Time is up, resize the bat back
            ReturnBatToOriginalSize();
        }

        // resize the bat over time. If the time for the changed size expires, return to normal size.

            var currentSize = BatBase.transform.localScale;
            var desiredSize = new Vector3(TargetBatSize, 1f, 1f);

            var result = Vector3.SmoothDamp(currentSize, desiredSize, ref currVelocity, 2f);
            BatBase.transform.localScale = result;
        print(currVelocity);       

    }

    public void AttachGun()
    {
        Gun.SetActive(true);
        currentAmmo = Ammo;
    }

    public void EnlargeBat()
    {
        BatSizeTime = Time.time;
        TargetBatSize = 3;
        // isResizing = true;
        AudioController.MainAudioController.PlaySound(BatExpandClip);
        print("Enlarge the bat");
    }

    private void ReturnBatToOriginalSize()
    {
        TargetBatSize = 2;
       // isResizing = true;
    }

    public void ShrinkBat()
    {
        BatSizeTime = Time.time;
        TargetBatSize = 1f;
        // isResizing = false;
        AudioController.MainAudioController.PlaySound(BatShrinkClip);
        print("Shrink the bat");
    }

    public void ActivateShield()
    {
        LeftSheild.SetActive(true);
        RightShield.SetActive(true);
        ShieldTime = Time.time;
        isShieldActive = true;
        AudioController.MainAudioController.PlaySound(ShieldSoundOnClip);
        Invoke("playLoopedShield", ShieldLoopSoundDelay );
        //AudioController.MainAudioController.PlaySound()
    }

    void playLoopedShield()
    {
        var ac = gameObject.GetComponent<AudioSource>();
        ac.loop = true;
        ac.PlayOneShot(ShieldLoopClip);
    }

    void stopLoopedShield()
    {
        var ac = gameObject.GetComponent<AudioSource>();
        ac.Stop();
    }

    private void deactivateShield()
    {
        if (isShieldActive)
        {
            stopLoopedShield();
            AudioController.MainAudioController.PlaySound(ShieldSoundOffClip);
        }

        LeftSheild.SetActive(false);
        RightShield.SetActive(false);
        isShieldActive = false;
        // Deactivate any sound playing
    }

    public void AddBall()
    {
        // Multi-ball
    }

    public void ResetBat()
    {
        deactivateShield();
        DeactivateGun();

        TargetBatSize = BatSize;
        
        BatBase.transform.localScale = new Vector3(TargetBatSize, 1f, 1f);


        // Remove any balls

        // Prepare first ball.
    }

    void ShowFakeBall()
    {
        FakeBall.SetActive(true);
        ball.SetActive(false);
    }

    void HideFakeBall()
    {
        FakeBall.SetActive(false);
        ball.SetActive(true);
    }

    void DeactivateGun()
    {
        Gun.SetActive(false);
        currentAmmo = 0;
    }

    private void FireGun()
    {
        if (currentAmmo > 0)
        {
            currentAmmo--;
            // Create an instance of the bullet at the position above the gun.
            var laser = Instantiate(BulletPrefab, gameObject.transform.position + BulletSpawnPoint, Quaternion.identity);
            var laserController = laser.GetComponent<LaserBoltController>();
            laserController.FireLaser(BulletSpeed);
            AudioController.MainAudioController.PlaySound(FireLaserSound);
        }
        else
        {
            DeactivateGun();
        }
        
    }

    private void processBallLaunch()
    {
        if(ball == null)
        {
          //  print("We have no ball");
            return;
        }

        bool fire = CrossPlatformInputManager.GetButtonDown("Fire");

        if (fire)
        {
            if (Gun.activeInHierarchy)
            {
                FireGun();
            }

            var ballController = ball.GetComponent<BallController>();

            Vector3 targetPos = ball.transform.position - transform.position;
            float angle = Vector3.Angle(transform.forward, targetPos);
            float launchAngle = angle - 180;

            print(angle);

            print("launch angle is:" + launchAngle.ToString());
            float xForce = LaunchForce * Mathf.Cos( Mathf.Deg2Rad * launchAngle );
            float yForce = LaunchForce * Mathf.Sin( Mathf.Deg2Rad * launchAngle );
            HideFakeBall();
            ballController.LaunchBall(new Vector3(xForce, yForce, 0f));
        }
    }


    public void ResetBatAndBall()
    {
        var ballController = ball.GetComponent<BallController>();
        ballController.RelocateBall(BallStartPosition, true );
        ballController.SetNormalMode();
        gameObject.transform.position = BatStartPosition;
        ShowFakeBall();
        ResetBat();
    }

    private void processTranslation()
    {
        float horizontalThrow = CrossPlatformInputManager.GetAxis("Horizontal");
        float xOffset = horizontalThrow * BatSpeed * Time.deltaTime;
        float rawXPosition = transform.localPosition.x + xOffset;
        float yPosition = transform.localPosition.y;
        float zPosition = transform.localPosition.z;

        rawXPosition = Mathf.Clamp(rawXPosition, MinPosition, MaxPosition);

        transform.localPosition = new Vector3(rawXPosition, yPosition, zPosition);
    }
}
