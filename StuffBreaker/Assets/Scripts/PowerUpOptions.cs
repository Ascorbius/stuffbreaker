﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PowerUpOptions
{
    None,
    GoldCoin,
    FreeDuckie,
    BatLarge,
    BatSmall,
    Treasure,
    MultiBall,
    BatCannon,
    BatShield,
    RageBall,
    GrowBall,
    ShrinkBall
}
