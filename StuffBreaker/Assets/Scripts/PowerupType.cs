﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/PowerupInfo", order = 1)]
[Serializable]
public class PowerupType : ScriptableObject
{
    public PowerUpOptions powerupKey;
    public GameObject powerupPrefab;
    public int DifficultyRating;
}
