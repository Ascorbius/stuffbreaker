﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{

    public void SetMainGame()
    {
        var sceneName = "MainGameArea";
        SceneManager.LoadScene(sceneName);
    }

    public void SetMainMenu()
    {
        var sceneName = "MainMenu";
        SceneManager.LoadScene(sceneName);
    }

    public void SetSelectLevel()
    {
        var sceneName = "LevelSelect";
        SceneManager.LoadScene(sceneName);
    }
}
