﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleGravity : MonoBehaviour
{
    [SerializeField] Vector3 MoveDirection = new Vector3(0f, -10f, 0f);

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position += ( MoveDirection * Time.deltaTime );
    }
}
