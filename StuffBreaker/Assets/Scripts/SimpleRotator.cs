﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleRotator : MonoBehaviour
{
    [SerializeField] Vector3 RotateBy = new Vector3(0f, 2f, 0f);

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var currentRotation = gameObject.transform.localRotation;
        gameObject.transform.localRotation = Quaternion.Euler(currentRotation.eulerAngles + ( RotateBy * Time.deltaTime ));
    }
}
