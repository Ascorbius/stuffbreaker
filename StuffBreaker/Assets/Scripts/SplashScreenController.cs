﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SplashScreenController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Invoke("GoToMainMenu", 1);
    }

    private void GoToMainMenu()
    {
        var sceneName = "MainMenu";
        SceneManager.LoadScene(sceneName);
    }

}
