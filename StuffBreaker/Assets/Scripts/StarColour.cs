﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StarColour
{
    None, Bronze, Silver, Gold
}
