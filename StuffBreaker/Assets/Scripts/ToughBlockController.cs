﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToughBlockController : BrickController
{
    [SerializeField] int HitCount = 2;
    [SerializeField] List<Material> BlockMaterials;
    int currentMaterialId = 0;
    // Start is called before the first frame update

    private void Awake()
    {
        setMaterial(currentMaterialId);
    }

    void Start()
    {
        
    }

    void setNextMaterial()
    {
        if(currentMaterialId < BlockMaterials.Count)
        {            
            setMaterial(currentMaterialId);
            currentMaterialId++;
            if(currentMaterialId>= BlockMaterials.Count)
            {
                currentMaterialId = BlockMaterials.Count - 1;
            }
        }
    }

    void setMaterial( int materialId )
    {
        var renderer = gameObject.GetComponent<MeshRenderer>();
        renderer.sharedMaterial = BlockMaterials[materialId];
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool IsDead()
    {
        return HitCount <= 0;
    }

    public override void OnCollisionEnter(Collision collision)
    {
        if (!HitByBall(collision) && !HitByLaser(collision))
        {
            return;
        }

        if (HitByWreckingBall(collision))
        {
            HitCount = 0;
        }
        else
        {
            HitCount--;
        }

        if (HitByLaser(collision))
        {
            HitCount--;
        }
        if (HitCount <= 0)
        {
            base.DestroyBlock();
        }
        else
        {
            setNextMaterial();
            PlayBounceSound();
        }

        if (HitByWreckingBall(collision))
        {
            var collider = gameObject.GetComponent<Collider>();
            if (collider != null)
            {
                Physics.IgnoreCollision(collision.collider, collider);
            }
        }
    }
}
