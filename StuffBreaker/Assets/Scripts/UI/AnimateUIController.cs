﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateUIController : MonoBehaviour
{
    [SerializeField] Vector3 StartingPosition;
    [SerializeField] float ProgressPerSecond;
    [SerializeField] Vector3 CurrentVelocity = new Vector3(0f,0f,0f);
    [SerializeField] int ElementNumber = 0;
    private Vector3 endPosition;
    [SerializeField] float progress = 0;
    [SerializeField] bool animationStarted = false;
    bool endPositionSet = false;
    //private Vector3 elementPosition;

    // Start is called before the first frame update
    void Start()
    {

    }

    public void ResetAnimation()
    {
        var starRect = gameObject.GetComponent<RectTransform>();
        if (!endPositionSet)
        {
            endPositionSet = true;
            endPosition = starRect.anchoredPosition;
            print(string.Format("Star {6} starting at x:{0} y:{1} - Moving to x:{2} y:{3} - Animating to x:{4} y:{5}",
                starRect.anchoredPosition.x, starRect.anchoredPosition.y,
                StartingPosition.x, StartingPosition.y,
                endPosition.x, endPosition.y, ElementNumber));
        }

        starRect.anchoredPosition = StartingPosition;

        animationStarted = false;


        starRect.anchoredPosition = StartingPosition;
        animationStarted = false;
    }

    public void StartAnimation()
    {
        animationStarted = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!animationStarted)
        {
            return;
        }
        progress = Mathf.Clamp(progress, 0f, 1f);
        var starRect = gameObject.GetComponent<RectTransform>();
        //gameObject.transform.position = Vector3.Lerp(StartingPosition, endPosition, progress);
        starRect.anchoredPosition = Vector3.SmoothDamp(starRect.anchoredPosition, endPosition, ref CurrentVelocity, ProgressPerSecond, 1000f, Time.unscaledDeltaTime);
    }
}
