﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelListController : MonoBehaviour
{
    [SerializeField] GameObject LevelButton;
    [SerializeField] GameObject ButtonCanvas;
    [SerializeField] int MaxCols = 4;
    [SerializeField] int MaxRows = 100;
    [SerializeField] Transform GridTransform;

    // Start is called before the first frame update
    void Start()
    {
        // Get the list of levels

        // Create the buttons for each level 
        var controller = GameStateController.MainGameStateController;
        var levels = GameStateController.MainGameStateController.GetLevels();
        
        
        var levelIndex = 0;
        foreach ( var level in levels)
        {
            var button = Instantiate(LevelButton, GridTransform);
            
            var levelSelect = button.GetComponent<LevelSelector>();

            var statsForLevel = GameStateController.MainGameStateController.GetLevelStatistics(level.LevelHash);
            if (statsForLevel == null)
            {
                statsForLevel = new LevelStatistics() { DuckiesMurdered = 9999, Score = 0, DuckyStar = StarColour.None, ComboStar = StarColour.None, TreasureStar = StarColour.None };
            }

            levelSelect.Initialise(levelIndex, level, statsForLevel);

            levelIndex++;
        }

    }

}
