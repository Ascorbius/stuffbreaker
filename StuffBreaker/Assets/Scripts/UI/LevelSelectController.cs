﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelectController : MonoBehaviour
{
    public void OpenLevelSelectScene()
    {
        var LevelSelectScene = SceneManager.GetSceneByName("LevelSelect");
        SceneManager.SetActiveScene(LevelSelectScene);
    }

    


}
