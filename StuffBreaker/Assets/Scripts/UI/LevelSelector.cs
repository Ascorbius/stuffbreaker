﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelSelector : MonoBehaviour
{
    public int levelIndex = 0;
    [SerializeField]public Text LevelNameText;
    [SerializeField]public Text LevelAuthorText;
    [SerializeField] public GameObject levelController;
    [SerializeField] public string Action;
    [SerializeField] public GameObject DuckyStar;
    [SerializeField] public GameObject ComboStar;
    [SerializeField] public GameObject TreasureStar;
    [SerializeField] public Text LevelHighScore;
    [SerializeField] public Text DeadDuckies;
    [SerializeField] public GameObject DeadDuck;


    private LevelData level;
    private LevelStatistics levelStats;

    // Start is called before the first frame update
    public void Initialise( int index,  LevelData levelData, LevelStatistics levelStatistics )
    {
        level = levelData;
        levelStats = levelStatistics;
        levelIndex = index;


        LevelNameText.text = level.LevelName;
        LevelAuthorText.text = level.LevelAuthor;

        if (levelStats != null)
        {

            LevelHighScore.text = levelStats.Score.ToString();
            LevelHighScore.gameObject.SetActive(levelStats.Score > 0);
            DeadDuckies.text = levelStats.DuckiesMurdered.ToString();
            DeadDuckies.gameObject.SetActive( levelStats.DuckiesMurdered > 0 && levelStats.DuckiesMurdered < 9999 );
            DeadDuck.SetActive(DeadDuckies.gameObject.activeInHierarchy);

            setStar(DuckyStar, levelStats.DuckyStar);
            setStar(ComboStar, levelStats.ComboStar);
            setStar(TreasureStar, levelStats.TreasureStar);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SelectLevel()
    {
        print("Level " + level.LevelName + " was selected");
        GameStateController.MainGameStateController.setCurrentLevel(levelIndex);        
    }

    public void StartLevel( )
    {
        var sceneName = "MainGameArea";

        SceneManager.LoadScene(sceneName);        
    }

    private void setStar( GameObject star, StarColour starColour)
    {
        var sc = star.GetComponent<StarController>();
       // var auic = star.GetComponent<AnimateUIController>();
        switch (starColour)
        {
            case StarColour.Bronze:
                sc.SetBronze();
                star.SetActive(true);
                break;
            case StarColour.Silver:
                sc.SetSilver();
                star.SetActive(true);
                break;
            case StarColour.Gold:
                sc.SetGold();
                star.SetActive(true);
                break;
            default:
                star.SetActive(false);
                break;
        }

    }
}
