﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StarController : MonoBehaviour
{
    [SerializeField] Material SilverMaterial;
    [SerializeField] Material BronzeMaterial;
    [SerializeField] Material GoldMaterial;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetGold()
    {
        var rawImage = gameObject.GetComponent<RawImage>();
        rawImage.color = GoldMaterial.color;
    }

    public void SetSilver()
    {
        var rawImage = gameObject.GetComponent<RawImage>();
        rawImage.color = SilverMaterial.color;
    }

    public void SetBronze()
    {
        var rawImage = gameObject.GetComponent<RawImage>();
        rawImage.color = BronzeMaterial.color;
    }
}
