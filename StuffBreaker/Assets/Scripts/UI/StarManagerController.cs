﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class StarManagerController : MonoBehaviour
{
    [SerializeField] GameObject Star1;
    [SerializeField] GameObject Star2;
    [SerializeField] GameObject Star3;
    [SerializeField] float StarTime = 0.3f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartStars( StarColour star1Colour, StarColour star2Colour, StarColour star3Colour)
    {
        var s1 = SetUpStar(Star1, star1Colour);
        var s2 = SetUpStar(Star2, star2Colour);
        var s3 = SetUpStar(Star3, star3Colour);
        s1.ResetAnimation();
        s2.ResetAnimation();
        s3.ResetAnimation();

        print("Starting CoRoutine");

        StartCoroutine(RewardStars(s1, s2, s3));
    }

    private IEnumerator RewardStars( AnimateUIController s1, AnimateUIController s2, AnimateUIController s3 )
    {
        print("Rewarding Stars");
        yield return new WaitForSecondsRealtime(StarTime);
        print("Rewarding Star 1");
        ActivateStar( s1 );
        yield return new WaitForSecondsRealtime(StarTime);
        print("Rewarding Star 2");
        ActivateStar( s2 );
        yield return new WaitForSecondsRealtime(StarTime);
        print("Rewarding Star 3");
        ActivateStar( s3 );
        yield break;
    }

    private void ActivateStar( AnimateUIController starAnimator)
    {
        //print("Activating a Star");
        starAnimator.StartAnimation();
    }

    private AnimateUIController SetUpStar( GameObject star, StarColour starColour )
    {
        var sc = star.GetComponent<StarController>();
        var auic = star.GetComponent<AnimateUIController>();
        switch( starColour)
        {
            case StarColour.Bronze:
                sc.SetBronze();
                star.SetActive(true);
                break;
            case StarColour.Silver:
                sc.SetSilver();
                star.SetActive(true);
                break;
            case StarColour.Gold:
                sc.SetGold();
                star.SetActive(true);
                break;
            default:
                star.SetActive(false);
                break;
        }

        return auic;
    }
}
