﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartButtonController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Pressed( int levelId )
    {
        var sceneName = "MainGameArea";
        //var LevelSelectScene = SceneManager.GetSceneByName("MainGameArea");
        var sceneList = SceneManager.GetAllScenes();
        foreach( var scene in sceneList)
        {
            print(scene.name);
        }
        SceneManager.LoadScene(sceneName);
    }
}
