﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterCollisionController : MonoBehaviour
{
    [SerializeField] GameObject WaterFX;
    [SerializeField] GameObject levelController;
    

    private void OnCollisionEnter(Collision collision)
    {        
        if (collision.gameObject.tag == "Ball" || collision.gameObject.tag == "Collectable" )
        {
            var fx = Instantiate( WaterFX, collision.gameObject.transform.position, Quaternion.identity);

            var controller = levelController.GetComponent<LevelController>();
            controller.Splosh();
        }
    }
}
