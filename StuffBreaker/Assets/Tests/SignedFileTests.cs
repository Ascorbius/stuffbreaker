﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
 


namespace Tests
{

   /* public class SignedFileTests
    {
        // A Test behaves as an ordinary method


        [Test]
        public void SignedFileTestsSimplePasses()
        {
            // Use the Assert class to test conditions
            var go = new GameObject("testObject");
            var gsc = go.AddComponent<GameStateController>();

            var originalText = "{\"SomeJson\":1234}";
            var signedText = gsc.SignText(originalText);
            var comparedText = gsc.TamperTest(signedText);
            Assert.AreEqual(originalText, comparedText);
        }*/


      /*  [Test]
        public void SigningReturnsValidJson()
        {
            var go = new GameObject("testObject");
            var gsc = go.AddComponent<GameStateController>();

            var savePayload = new SavePayload();
            savePayload.FileSaveDate = System.DateTime.Now;
            savePayload.LevelStatistics = new List<LevelStatistics>();
            savePayload.LevelStatistics.Add(new LevelStatistics() { BallMisses = 100, BulletsFired = 100, LevelHash = "1234" });

            var json = JsonConvert.SerializeObject(savePayload);

            var signedText = gsc.SignText(json);
            var compareText = gsc.TamperTest(signedText);

            var obj = JsonConvert.DeserializeObject<SavePayload>(compareText);
            Assert.IsNotNull(obj);
        }*/

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
       /* [UnityTest]
        public IEnumerator SignedFileTestsWithEnumeratorPasses()
        {
            // Use the Assert class to test conditions.
            // Use yield to skip a frame.
            yield return null;
        }*/
  //  }
}
